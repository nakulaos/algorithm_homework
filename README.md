# 1. 运行
```bash
npm install
npm run dev
```

# 2. 项目结构
```
.
├── README.md
├── env.d.ts
├── index.html
├── package-lock.json
├── package.json
├── public
│   └── favicon.ico
├── src
│   ├── App.vue // 根组件
│   ├── assets
│   │   ├── base.css
│   │   ├── eightqueen.png
│   │   ├── logo.svg
│   │   └── main.css
│   ├── components
│   ├── layout
│   │   ├── Layout.vue // 全局布局组件
│   │   └── Sider
│   │       └── Sider.vue // 侧边栏组件
│   ├── main.ts
│   ├── router
│   │   └── index.ts // 路由
│   ├── stores
│   │   └── counter.ts
│   └── views
│       ├── BeiBao.vue   // 背包问题
│       ├── EightQueenView.vue // 八皇后问题
│       └── SaoleiView.vue // 扫雷问题
├── tsconfig.app.json
├── tsconfig.json
├── tsconfig.node.json
└── vite.config.ts

```
