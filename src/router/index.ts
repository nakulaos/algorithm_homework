import { createRouter, createWebHistory } from 'vue-router'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/SaoleiView.vue')
    },
    {
      path: '/saolei',
      name: 'saolei',
      component: () => import('../views/SaoleiView.vue')
    },
    {
      path:"/eight-queens",
      name:"eight-queens",
      component:()=>import('../views/EightQueenView.vue')
    },
    {
      path:"/beibao",
      name:"beibao",
      component:()=>import('../views/BeiBao.vue')
    },
  ]
})

export default router
